package com.example.flowable.config;

import com.google.common.base.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.Inet4Address;
import java.net.UnknownHostException;

@Configuration
@EnableSwagger2
@Slf4j
public class SwaggerConfig implements WebMvcConfigurer , ApplicationListener<WebServerInitializedEvent> {

	@Bean
	public Docket createRestApi() {
		String packageName = "com.example.flowable.controller";
		Predicate<RequestHandler> basePackage = RequestHandlerSelectors.basePackage(packageName);

		ApiInfoBuilder builder = new ApiInfoBuilder();
		builder.title("流程审批服务API文档");
		builder.description("基于Flowable6.6.0提供的流程在线审批服务API文档");
		builder.termsOfServiceUrl("http://127.0.0.1:9080");
		builder.version("1.0");

		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket.enable(true);
		docket.groupName("API接口文档");
		docket.apiInfo(builder.build());
		docket.select().apis(basePackage).paths(PathSelectors.any()).build();

		return docket;
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public void onApplicationEvent(WebServerInitializedEvent event) {
		try {
			// 获取IP
			String hostAddress = Inet4Address.getLocalHost().getHostAddress();
			// 获取端口号
			int port = event.getWebServer().getPort();
			// 获取应用名
			String applicationName = event.getApplicationContext().getApplicationName();
			// 打印 swagger 文档地址
			log.info("项目启动启动成功！swagger 接口文档地址: http://" + hostAddress + ":" + port + applicationName + "/swagger-ui.html");
			// 打印 swagger2 文档地址
			log.info("项目启动启动成功！swagger2 接口文档地址: http://" + hostAddress + ":" + port + applicationName + "/doc.html");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
}
