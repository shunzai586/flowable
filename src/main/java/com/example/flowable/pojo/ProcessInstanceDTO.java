package com.example.flowable.pojo;

import lombok.Data;

@Data
public class ProcessInstanceDTO {
	String processInstanceId;
	String processDeploymentId;
}
