package com.example.flowable.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HistoryProcessDTO {
	String processInstanceId;
	String taskId;
	Date startTime;
	Date endTime;
}
