package com.example.flowable.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class TaskInstanceDTO {
	private String taskId;
	private String taskName;
	private String processInstanceId;
	private String requestUser;
	private String resourceId;
	private Date createTime;
}
