package com.example.flowable.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class ProcessStatusDTO {
	//String taskId;
	String taskName;
	String assignee;
	Date createTime;
	String approved;
	String comment;
}
